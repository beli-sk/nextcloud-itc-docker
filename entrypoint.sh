#!/bin/sh
cat > /etc/php/7.3/apache2/conf.d/redis-session.ini <<_EOF
session.save_handler = redis
session.save_path = "tcp://${REDIS_HOST}:${REDIS_HOST_PORT:=6379}?auth=${REDIS_PASSWORD}"
_EOF
exec /usr/sbin/apachectl -D FOREGROUND
