FROM debian:buster-slim
MAINTAINER Michal Belica <code@beli.sk>
EXPOSE 80 443

ENV NEXTCLOUD_VERSION 16.0.7
ENV NEXTCLOUD_VERSION_FULL 16.0.7.1
ENV CALENDAR_VERSION v1.7.0
ENV CONTACTS_VERSION v3.1.3

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
		php7.3 \
		apache2 \
		libapache2-mod-php7.3 \
		php7.3-xml \
		php7.3-gd \
		php7.3-json \
		php7.3-mbstring \
		php7.3-zip \
		php7.3-mysql \
		php7.3-curl \
		php7.3-bz2 \
		php7.3-intl \
		php7.3-sqlite3 \
		php-redis \
		ca-certificates \
		sudo \
		curl \
		bzip2 \
	&& rm -rf /var/www/html/* \
	&& curl -o nextcloud.tar.bz2 https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2 \
	&& tar -C /var/www/html/ --strip-components=1 --no-same-owner --no-same-permissions -jxf nextcloud.tar.bz2 \
	&& mkdir /var/www/html/data \
	&& rm -v nextcloud.tar.bz2 \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*



COPY config.php /var/www/html/config/config.php
COPY vhost.conf /etc/apache2/sites-enabled/
COPY htaccess /var/www/html/.htaccess
COPY entrypoint.sh /

RUN curl -L -O https://github.com/nextcloud/calendar/releases/download/${CALENDAR_VERSION}/calendar.tar.gz \
	&& tar -C /var/www/html/apps/ --no-same-owner --no-same-permissions -zxf calendar.tar.gz \
	&& rm calendar.tar.gz \
	&& curl -L -O https://github.com/nextcloud/contacts/releases/download/${CONTACTS_VERSION}/contacts.tar.gz \
	&& tar -C /var/www/html/apps/ --no-same-owner --no-same-permissions -zxf contacts.tar.gz \
	&& rm contacts.tar.gz \
	&& chown -R root:root /var/www/html/config \
	&& touch /var/www/html/data/.ocdata \
	&& chown -R www-data: /var/www/html/data \
	&& ln -s /dev/stderr /var/www/html/data/nextcloud.log \
	&& ln -sf /dev/stderr /var/log/apache2/error.log \
	&& ln -sf /dev/stdout /var/log/apache2/access.log \
	&& a2dissite 000-default \
	&& a2enmod rewrite headers env dir mime \
	&& chown root:root /entrypoint.sh && chmod 0755 /entrypoint.sh

CMD ["/entrypoint.sh"]
