<?php

$NEXTCLOUD_HOST = getenv('NEXTCLOUD_HOST') ?: 'localhost';
$NEXTCLOUD_PROTO = getenv('NEXTCLOUD_PROTO') ?: 'http';

$CONFIG = array(
	'trusted_domains' => array($NEXTCLOUD_HOST),
	'datadirectory' => '/var/www/html/data',
	'dbtype' => getenv('DB_TYPE') ?: 'mysql',
	'dbhost' => getenv('DB_HOST') ?: '',
	'dbname' => getenv('DB_NAME') ?: 'nextcloud',
	'dbuser' => getenv('DB_USER') ?: '',
	'dbpassword' => getenv('DB_PASS') ?: '',
	'dbtableprefix' => getenv('DB_PREFIX') ?: '',
	'knowledgebaseenabled' => false,
	'remember_login_cookie_lifetime' => 60*60*24*15,
	'session_keepalive' => false,
	'token_auth_enforced' => true,
	'skeletondirectory' => '',
	'lost_password_link' => '',
	'mail_domain' => explode(':', $NEXTCLOUD_HOST)[0],
	//'mail_from_address' => 'nextcloud',
	'mail_smtpmode' => 'smtp',
	'mail_smtphost' => getenv('MAIL_HOST') ?: '',
	'mail_smtptimeout' => 10,
	'mail_smtpsecure' => '',
	'mail_smtpauth' => false,
	'mail_smtpauthtype' => 'LOGIN',
	'mail_smtpname' => '',
	'mail_smtppassword' => '',
	'mail_send_plaintext_only' => true,
	'overwritehost' => $NEXTCLOUD_HOST,
	'overwriteprotocol' => $NEXTCLOUD_PROTO,
	'overwritewebroot' => '/',
	'overwrite.cli.url' => $NEXTCLOUD_PROTO.'://'.$NEXTCLOUD_HOST.'/',
	'check_for_working_htaccess' => true,
	'htaccess.RewriteBase' => '/',
	'config_is_read_only' => true,
	'appstoreenabled' => false,

	'version' => getenv('NEXTCLOUD_VERSION_FULL'),
	'installed' => true,
	'secret' => getenv('SECRET'),
	'instanceid' => getenv('INSTANCE_ID'),
	
	'objectstore' => array (
		'class' => '\\OC\\Files\\ObjectStore\\S3',
		'arguments' => 
		array (
			'hostname' => getenv('S3_HOST'),
			'bucket' => getenv('S3_BUCKET'),
			'autocreate' => true,
			'key' => getenv('S3_ID'),
			'secret' => getenv('S3_SECRET'),
			'use_ssl' => true,
			'region' => getenv('S3_REGION'),
		),
	),
	'memcache.distributed' => '\\OC\\Memcache\\Redis',
	'memcache.locking' => '\\OC\\Memcache\\Redis',
	'redis' => array (
		'host' => getenv('REDIS_HOST'),
		'port' => getenv('REDIS_HOST_PORT') ?: '6379',
		'password' => getenv('REDIS_PASSWORD') ?: '',
	),
);

unset($NEXTCLOUD_HOST, $NEXTCLOUD_PROTO);
